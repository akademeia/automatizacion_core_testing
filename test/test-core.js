'use strict'
process.env.NODE_ENV = 'test';

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../app')
const should = chai.should()
const config = require('../config')
const j5 = require('../arduino')

let token = ''
let tokenInvalido = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJELTQwMSIsImNyZWF0ZWRfYXQiOjE0OTg2OTM2NDM10.0wooFaKohOzhJOtyqE8W9SobDlbm7Gm_IoAT_dFQER0'
let codigoIr = '0000 0069 0007 0000 032a 010e 005a 010e 005a 005a 0168 005a 005a 010e 010e 005a 005a 0168'

chai.use(chaiHttp)

describe('/signin', () => {
    it('Obtener token de autenticacion', (done) => {
        chai.request(server)
        .post('/signin')
        .send({
            idSalon: 'EN-501', 
            key: config.key
        })
        .end( (err, res) => {
            res.should.have.status(201)
            res.should.be.json
            res.body.should.have.property('token')
            res.body.token.should.be.a('string')
            //guardar el token obtenido para futuras peticiones
            token = res.body.token
            done()
        })
    })
    it('Obtener mensaje de error - falta parametro "key"', (done) => {
        chai.request(server)
        .post('/signin')
        .send({ 
            idSalon: 'EN-501' 
        })
        .end( (err, res) => {
            res.should.have.status(401)
            res.should.be.json
            res.body.should.have.property('message')
            res.body.message.should.be.a('string')
            res.body.message.should.equal('se requiere key para crear token')           
            done()
        })
    })
    it('Obtener mensaje de error - falta parametro "idSalon"', (done) => {
        chai.request(server)
        .post('/signin')
        .send({ 
            key: config.key 
        })
        .end( (err, res) => {
            res.should.have.status(401)
            res.should.be.json
            res.body.should.have.property('message')
            res.body.message.should.be.a('string')
            res.body.message.should.equal('se require id de salon')          
            done()
        })
    })
    it('Obtener mensaje de error - "key" incorrecto', (done) => {
        chai.request(server)
        .post('/signin')
        .send({
            idSalon: 'EN-501', 
            key: 'password'
        })
        .end( (err, res) => {
            res.should.have.status(401)
            res.should.be.json
            res.body.should.have.property('message')
            res.body.message.should.be.a('string')
            res.body.message.should.equal('key incorrecto')          
            done()
        })
    })
})

describe('middleware', () => {
    it('autenticacion middleware con token', (done) => {
        chai.request(server)
        .get('/api/prueba')
        .set('authorization', token)
        .end( (err, res) => {
            res.should.have.status(200)
            res.should.be.json
            res.body.should.have.property('message')
            res.body.message.should.be.a('string')
            res.body.message.should.equal('Prueba de envio de datos')          
            done()
        })
    })
    it('autenticacion middleware sin token', (done) => {
        chai.request(server)
        .get('/api/prueba')
        .end( (err, res) => {
            res.should.have.status(403)
            res.should.be.json
            res.body.should.have.property('message')
            res.body.message.should.be.a('string')
            res.body.message.should.equal('sin autorizacion')          
            done()
        })
    })
    it('autenticacion middleware con token invalido', (done) => {
        chai.request(server)
        .get('/api/prueba')
        .set('authorization', tokenInvalido)
        .end( (err, res) => {
            res.should.have.status(401)
            res.should.be.json
            res.body.should.have.property('message')
            res.body.message.should.be.a('string')
            res.body.message.should.equal('token invalido')          
            done()
        })
    })
})

describe('/ir', () => {
    it('falta parametro codigo en la peticion', (done) => {
        chai.request(server)
        .post('/api/ir')
        .set('authorization', token)
        .end((err, res) => {
            res.should.have.status(401)
            res.should.be.json
            res.body.should.have.property('message')
            res.body.message.should.be.a('string')
            res.body.message.should.equal('se requiere codigo Ir')          
            done()
        })
    })
    it('ejecutar codigo ir', (done) => {
        chai.request(server)
        .post(`/api/ir/${codigoIr}`)
        .set('authorization', token)
        .end((err, res) => {
            if(res.status == 500){
                console.log('driver o emisor de IR faltante')
                done()
            }
            res.should.have.status(200)
            res.should.be.json
            res.body.should.have.property('message')
            res.body.message.should.be.a('string')
            res.body.message.should.equal('codigo ejecutado')          
            done()
        })
    })
})