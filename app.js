'use strict'

const cors = require("cors");
const express = require('express')
const bodyParser = require('body-parser')

const auth = require('./middlewares/auth')
const log = require('./middlewares/log')
const app = express()
const api = require('./routes')
const authCtrl = require('./controllers/auth')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.post('/signin', authCtrl)
app.use('/api', auth, api)


module.exports = app