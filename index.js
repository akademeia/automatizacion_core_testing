'use strict'

const app = require('./app')
const config = require('./config')
let fs = require('fs')

app.listen(config.port, () => {
    console.log(`App escuchando en el puerto: ${config.port}`)
    console.log(`Tiempo mitad de la cortina: ${config.tiempo_cortinas}`);
    console.log(`Tiempo total de la cortina: ${config.tiempoMitad_cortinas}`);
})
