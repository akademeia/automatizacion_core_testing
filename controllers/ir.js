'use strict'

const exec = require('child_process').exec
const config = require('../config')
const uutx = '\\assets\\exe\\uutx.exe'


function executeCodeIr(req, res) {
    if (!req.body.codigo) return res.status(401).send({ message: 'se requiere codigo Ir' })
    let comando = `${config.proyect_location}${uutx} "${req.body.codigo}"`
    exec(comando, (err, stdout, stderr) => {
        if (err) {
            console.log(`--IR ${err}`)
            return res.status(500).send({ error: err })
        }
        return res.status(200).send({ message: 'codigo ejecutado' })

    });
}

module.exports = executeCodeIr