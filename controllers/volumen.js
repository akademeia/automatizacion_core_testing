"use strict"
const config = require('../config')
const serialport = require("serialport");
const queue = require('queue')
const q = queue()
let tiempo = ''

function VolumenController(request, response) {

    const port = config.COM_volumen;
    q.end()
    q.push(
        function (cb) {
            clearTimeout(tiempo)
            tiempo = setTimeout(function () {
                const portConfig = {
                    baudRate: 19200,
                    dataBits: 8,
                    stopBits: 2,
                    parity: 'none'
                };
               
                const serial = new serialport(port, portConfig);
                const exeCode = [ 40, 75, 69, 89, 54, 41 ];

                serial.on("error", function(error) {
                    /*return response.status(500).json({
                        mensaje: "COM Incorrecto!",
                        error: error,
                    });*/
                });
            
                serial.on("open", () => {
                    serial.write(exeCode, (err, res) => {
                        if (err) {
                            /*return response.status(500).json({
                                mensaje: "Hubo un error al ejecutar el codigo, por favor contacte al administrador del sistema",
                                data: body,
                                error: err
                            });*/
                        }
                        serial.close((err) => {
                           /* if (err) {
                                return response.status(500).json({
                                    mensaje: "Hubo un error al ejecutar el codigo, por favor contacte al administrador del sistema",
                                    data: body,
                                    error: err
                                });
                            }
                            return response.status(200).json({
                                mensaje: "El codigo esc de ejecuto correctamente",
                                puerto: port
                            });*/
                        });
                    });
                });
                cb()
            }, 5000)
        }
    ) 
    q.start(function (err) {
        if (err) throw err
    });
     return response.status(200).json({
        mensaje: "El codigo esc se puso en cola correctamente",
        puerto: port
    });
}

module.exports = VolumenController;