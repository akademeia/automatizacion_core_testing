'use strict'

const todas = require('../arduino/cortinas/todas')
const frontales = require('../arduino/cortinas/frontales')
const laterales = require('../arduino/cortinas/laterales')
const traseras = require('../arduino/cortinas/traseras')
const gradas = require('../arduino/cortinas/gradas')

function CortinaController (req, res) {

    switch(req.params.idCortina){

        case 'todas':
            todas(req.params.accion)
            .then( response => {
                return res.status(200).send({ message: 'ok' })
            })
            .catch( response => {
                return res.status(response.status).send({ message: response.message })
            })
            break

        case 'frontales':
            frontales(req.params.accion)
            .then( response => {
                return res.status(200).send({ message: 'ok' })
            })
            .catch( response => {
                return res.status(response.status).send({ message: response.message })
            })
            break

        case 'laterales':
            laterales(req.params.accion)
            .then( response => {
                return res.status(200).send({ message: 'ok' })
            })
            .catch( response => {
                return res.status(response.status).send({ message: response.message })
            })
            break

        case 'traseras': 
            traseras(req.params.accion)
            .then( response => {
                return res.status(200).send({ message: 'ok' })
            })
            .catch( response => {
                return res.status(response.status).send({ message: response.message })
            })
            break
        
        case 'gradas':
            gradas(req.params.accion)
            .then( response => {
                return res.status(200).send({ message: 'ok'})
            })
            .catch( response => {
                return res.status(response.status).send({ message: response.message })
            })
            break
        
        default:
            return res.status(400).send({ message: 'idCortina no disponible' })
            
    }
}

module.exports = CortinaController 