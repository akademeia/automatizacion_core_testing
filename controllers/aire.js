'use strict'

const uutx = '\\assets\\exe\\uutx.exe'
const config = require('../config')
const fs = require('fs');


function readFile(fileName) {
    return new Promise((resolve,reject) => {
        fs.readFile(fileName, (err, data) => {  
            if (err) return reject(err);
            resolve(data);
        });
    });
}

function writeFile(fileName,data) {
    return new Promise((resolve,reject) => {
        fs.writeFile(fileName,data,'utf-8',(err,data) => {
            if(err) reject(err);
            resolve(data);
        });
    });
}


function ejecutarCodigo(type, cantidad, res) {
    if (cantidad != 0) {
        if(type == "subir") {
            return new Promise((resolve, reject) => {
                let comando = `${config.proyect_location}${uutx} "0000 006B 0000 0063 00EC 011F 0015 0041 0015 0040 0015 0040 0015 0040 0015 0041 0015 0040 0015 0040 0015 0040 0015 0017 0015 0018 0015 0016 0015 0017 0015 0016 0015 0017 0015 0016 0015 0018 0015 0041 0015 0040 0015 0040 0015 0041 0015 0040 0015 0040 0015 0040 0015 0040 0015 0017 0015 0016 0015 0017 0015 0017 0015 0017 0015 0016 0015 0018 0015 0016 0015 0017 0015 0016 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 0041 0015 0016 0015 0017 0015 0016 0015 0017 0015 0017 0015 0017 0015 0040 0015 0016 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 0041 0015 0017 0015 0040 0015 0016 0015 0017 0015 0016 0015 0018 0015 0016 0015 0017 0015 0017 0015 0040 0015 0016 0015 0040 0015 0040 0015 0016 0015 0040 0015 0040 0015 0040 0015 0017 0015 0040 0015 0018 0015 0017 0015 0040 0015 0016 0015 0017 0015 0017 0015 0040 0015 0018 0015 0040 0015 0016 0015 0040 0015 0017 0015 0017 0015 0040 0015 0017 0015 0041 0015 0018 0015 0040 0015 0016 0015 0040 0015 0040 0015 011E 0015 0FCB"`
                let exec = require('child_process').exec;
                exec(comando, (err, stdout, stderr) => {
                    if (err) return resolve(false);
                    if (stderr) return resolve(false);
                    cantidad = cantidad - 1;
                    return resolve(ejecutarCodigo(type, cantidad, res));
                });
            });
        } else if (type == "bajar") {
            return new Promise((resolve, reject) => {
                let comando = `${config.proyect_location}${uutx} "0000 006C 0000 0063 00EA 011C 0015 0040 0015 003F 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 0017 0015 0016 0015 0016 0015 0016 0015 0016 0015 0016 0015 0016 0015 0016 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 003F 0015 0040 0015 003F 0015 0017 0015 0016 0015 0015 0015 0016 0015 0016 0015 0016 0015 0016 0015 0016 0015 003F 0015 0016 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 0040 0015 0017 0015 0040 0015 0015 0015 0016 0015 0016 0015 0016 0015 0016 0015 0016 0015 0040 0015 0016 0015 0016 0015 003F 0015 0040 0015 0040 0015 003F 0015 0040 0015 0017 0015 003F 0015 0040 0015 0016 0015 0016 0015 0016 0015 0016 0015 0016 0015 0017 0015 003F 0015 003F 0015 0016 0015 003F 0015 0040 0015 0040 0015 0040 0015 0040 0015 0016 0015 0016 0015 003F 0015 0016 0015 0016 0015 0016 0015 0016 0015 0017 0015 003F 0015 0016 0015 003F 0015 0016 0015 003F 0015 0016 0015 0016 0015 0040 0015 0017 0015 003F 0015 0016 0015 003F 0015 0016 0015 003F 0015 0040 0015 011C 0015 15D4"`
                let exec = require('child_process').exec;
                exec(comando, (err, stdout, stderr) => {
                    if (err) return resolve(false);
                    if (stderr) return resolve(false);
                    cantidad = cantidad - 1;
                    return resolve(ejecutarCodigo(type, cantidad, res));
                });
            });
        } 
    } else {
        return new Promise((resolve, reject) => {
            return resolve(true);
        })
    }
}

function AireController (req, res) {
    var cantidadGradosBd = 20;
    var cantidad = 0;
    var qh = null;
    if (!req.params.cantidad) return res.status(401).send({ message: 'se requiere cantidad grados' })
    var cantidadGrados = parseInt(req.params.cantidad);
    readFile(`${config.proyect_location}/aire.json`).then(data => { 
        cantidadGradosBd = JSON.parse(data);
        cantidadGradosBd = cantidadGradosBd.cantidad;
        return cantidadGradosBd;
    }).then(cantidadGradosBd => {
        var cantidad = 0;
        var codigoAEjecutar = null;
        if(cantidadGradosBd >= cantidadGrados)
        {
            cantidad = cantidadGradosBd - cantidadGrados;
            codigoAEjecutar = "subir";
        }
        else if(cantidadGrados >= cantidadGradosBd)
        {
            cantidad = cantidadGrados - cantidadGradosBd;
            codigoAEjecutar = "bajar";
        }
        return (ejecutarCodigo(codigoAEjecutar, cantidad, res))
    }).then(escribir => {
        return writeFile(`${config.proyect_location}/aire.json`,JSON.stringify({cantidad: cantidadGrados}));
    }).then(result => {
        res.status(200).send({
            message : "Correcto!"
        })   
    }).catch(error => {
        res.status(500).send({
            message : error
        }) 
    })
}

module.exports = AireController     