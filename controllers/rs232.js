"use strict"

const serialport = require("serialport");

const rs232Controller = {
    sendCommand: function(request, response) {
        const port = request.params.code;
        const body = request.body;
        if (!port.includes("COM")) {
            console.log('No tiene COM configurado');
            return response.status(404).send("Puerto o codigo incorrecto");
        }
        let exeCode = new Array();
        body.code.split(",").map((value) => {
            exeCode.push(parseInt(value, 16));
        });

        let portConfig = {
            baudRate: parseInt(body.baudRate),
            dataBits: parseInt(body.dataBits),
            stopBits: parseInt(body.stopBits),
            parity: body.parity == 0 ? 'none' : body.parity == 1 ? 'mark' : body.parity == 2 ? 'even' : body.parity == 3 ? 'odd' : 'space'

        };

        //Ejecutar codigo
        const serial = new serialport(port, portConfig);

        serial.on("error", function(error) {
            console.log('Error serial 1 - '+error);
            serial.close();
            return response.status(500).json({
                mensaje: "COM Incorrecto!",
                error: error,
            });

        });

        serial.on("open", () => {
            serial.write(exeCode, (err, res) => {
                if (err) {
                    console.log('Error serial 2 - '+err);
                    serial.close();
                    return response.status(500).json({
                        mensaje: "Hubo un error al ejecutar el codigo, por favor contacte al administrador del sistema",
                        data: body,
                        error: err
                    });
                }
                serial.close((err) => {
                    if (err) {
                        console.log('Error serial 3 - '+err);
                        serial.close();
                        return response.status(500).json({
                            mensaje: "Hubo un error al ejecutar el codigo, por favor contacte al administrador del sistema",
                            data: body,
                            error: err
                        });
                    }
                    return response.status(200).json({
                        mensaje: "El codigo se ha ejecutado correctamente",
                        puerto: port
                    });
                });

            });
        });
    }
};

module.exports = rs232Controller;