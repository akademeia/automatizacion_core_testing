'use strict'

const moment = require('moment')
const request  = require('request')
const config = require('../config')

function log(req, res, next){

    // console.log("Enviado request");

    let formData = {
        idAutomatizacion: req.idAutomatizacion,
        ruta: req.originalUrl,
        created_at: moment().unix()
    }
    

    var options = {
        host : `${config.rutaLog}?key=C3t@`,
        method : "POST"

    };

    request.post({url: `${config.rutaLog}?key=C3t@` , formData: formData},function(err){
        if(err) {
            console.log("Log no insertado!",err);
        } else {
            console.log("API Call Correct",options,formData);
        }
    })

    next();
}

module.exports = log