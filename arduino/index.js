'use strict'

let relays
const five = require('johnny-five')
const config = require('../config')

const board = new five.Board({
    port : config.COM_arduino,
    repl: false
})

board.on('ready', () => {

    board.pinMode(13, 1) 
    board.pinMode(12, 1) 
    board.pinMode(11, 1)
    board.pinMode(10, 1)
    board.pinMode(9, 1)
    board.pinMode(8, 1)
    board.pinMode(7, 1)
    board.pinMode(6, 1)
    relays = new five.Relays(config.pines_arduino)
    relays.close()


})
module.exports = {
    allUpON: () => {
        relays[0].open()
        relays[2].open()
        relays[4].open()
        relays[6].open()
    },
    allUpOFF: () => {
        relays[0].close()
        relays[2].close()
        relays[4].close()
        relays[6].close()
    },
    allDownON: () => {
        relays[1].open()
        relays[3].open()
        relays[5].open()
        relays[7].open()
    },
    allDownOFF: () => {
        relays[1].close()
        relays[3].close()
        relays[5].close()
        relays[7].close()
    },
    frontUpON: () => {
        relays[0].open()
    },
    frontUpOFF: () => {
        relays[0].close()        
    },
    frontDownON: () => {
        relays[1].open()        
    },
    frontDownOFF: () => {
        relays[1].close()        
    },
    sideUpON: () => {
        relays[2].open()
    },
    sideUpOFF: () => {
        relays[2].close()        
    },
    sideDownON: () => {
        relays[3].open()        
    },
    sideDownOFF: () => {
        relays[3].close()        
    },
    rearUpON: () => {
        relays[4].open()
    },
    rearUpOFF: () => {
        relays[4].close()        
    },
    rearDownON: () => {
        relays[5].open()        
    },
    rearDownOFF: () => {
        relays[5].close()        
    },
    stepsUpON: () => {
        relays[6].open()
    },
    stepsUpOFF: () => {
        relays[6].close()        
    },
    stepsDownON: () => {
        relays[7].open()        
    },
    stepsDownOFF: () => {
        relays[7].close()        
    }
}

