'use strict'

const arduino = require('../')
const config = require('../../config')
const LocalStorage = require('node-localstorage').LocalStorage
const async = require('async').parallel

let storageFrontales = LocalStorage(`${config.proyect_location}/storage/storage_cortinas_frontales`)
let storageLaterales = LocalStorage(`${config.proyect_location}/storage/storage_cortinas_laterales`)
let storageTraseras = LocalStorage(`${config.proyect_location}/storage/storage_cortinas_traseras`)
let storageGradas = LocalStorage(`${config.proyect_location}/storage/storage_cortinas_gradas`)



function cortina(accion) {

    return new Promise((resolve, reject) => {

        try {

            switch (accion) {

                case 'subir':

                    async({
                        frontales: (callback) => {
                            if (storageFrontales.getItem('estado') == 0.5) {
                                console.log('cortinas - subiendo frontales desde mitad')
                                arduino.frontUpON()
                                setTimeout(() => {
                                    arduino.frontUpOFF()
                                    storageFrontales.setItem('estado', 1)
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageFrontales.getItem('estado') == 0) {
                                console.log('cortinas - subiendo frontales')
                                arduino.frontUpON()
                                setTimeout(() => {
                                    arduino.frontUpOFF()
                                    storageFrontales.setItem('estado', 1)
                                    callback(null, true)
                                }, config.tiempo_cortinas)
                            } else if (storageFrontales.getItem('estado') == 1) {
                                callback(null, true)
                            }
                        },
                        laterales: (callback) => {
                            if (storageLaterales.getItem('estado') == 0.5) {
                                console.log('cortinas - subiendo laterales desde mitad')
                                arduino.sideUpON()
                                setTimeout(() => {
                                    arduino.sideUpOFF()
                                    storageLaterales.setItem('estado', 1)
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageLaterales.getItem('estado') == 0) {
                                console.log('cortinas - subiendo laterales')
                                arduino.sideUpON()
                                setTimeout(() => {
                                    arduino.sideUpOFF()
                                    storageLaterales.setItem('estado', 1)
                                    callback(null, true)
                                }, config.tiempo_cortinas)
                            } else if (storageLaterales.getItem('estado') == 1) {
                                callback(null, true)
                            }
                        },
                        traseras: (callback) => {
                            if (storageTraseras.getItem('estado') == 0.5) {
                                console.log('cortinas - subiendo traseras desde mitad')
                                arduino.rearUpON()
                                setTimeout(() => {
                                    arduino.rearUpOFF()
                                    storageTraseras.setItem('estado', 1)
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageTraseras.getItem('estado') == 0) {
                                console.log('cortinas - subiendo traseras')
                                arduino.rearUpON()
                                setTimeout(() => {
                                    arduino.rearUpOFF()
                                    storageTraseras.setItem('estado', 1)
                                    callback(null, true)
                                }, config.tiempo_cortinas)
                            } else if (storageTraseras.getItem('estado') == 1) {
                                callback(null, true)
                            }
                        },
                        gradas: (callback) => {
                            if (storageGradas.getItem('estado') == 0.5) {
                                console.log('cortinas - subiendo gradas desde mitad')
                                arduino.stepsUpON()
                                setTimeout(() => {
                                    arduino.stepsUpOFF()
                                    storageGradas.setItem('estado', 1)
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageGradas.getItem('estado') == 0) {
                                console.log('cortinas - subiendo gradas')
                                arduino.stepsUpON()
                                setTimeout(() => {
                                    arduino.stepsUpOFF()
                                    storageGradas.setItem('estado', 1)
                                    callback(null, true)
                                }, config.tiempo_cortinas)
                            } else if (storageGradas.getItem('estado') == 1) {
                                callback(null, true)
                            }
                        }
                    }, (err, result) => {
                        if (err) reject()
                        if (result.frontales && result.laterales && result.traseras && result.gradas) resolve()
                        else reject()
                    })

                    break

                case 'bajar':

                    async({
                        frontales: (callback) => {
                            if (storageFrontales.getItem('estado') == 0.5) {
                                console.log('cortinas - bajando frontales desde la mitad')
                                arduino.frontDownON()
                                setTimeout(() => {
                                    arduino.frontDownOFF()
                                    storageFrontales.setItem('estado', 0)
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageFrontales.getItem('estado') == 1) {
                                console.log('cortinas - bajando frontales')
                                arduino.frontDownON()
                                setTimeout(() => {
                                    arduino.frontDownOFF()
                                    storageFrontales.setItem('estado', 0)
                                    callback(null, true)
                                }, config.tiempo_cortinas)
                            } else if (storageFrontales.getItem('estado') == 0) {
                                callback(null, true)
                            }
                        },
                        laterales: (callback) => {
                            if (storageLaterales.getItem('estado') == 0.5) {
                                console.log('cortinas - bajando laterales desde la mitad')
                                arduino.sideDownON()
                                setTimeout(() => {
                                    arduino.sideDownOFF()
                                    storageLaterales.setItem('estado', 0)
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageLaterales.getItem('estado') == 1) {
                                console.log('cortinas - bajando laterales')
                                arduino.sideDownON()
                                setTimeout(() => {
                                    arduino.sideDownOFF()
                                    storageLaterales.setItem('estado', 0)
                                    callback(null, true)
                                }, config.tiempo_cortinas)
                            } else if (storageLaterales.getItem('estado') == 0) {
                                callback(null, true)
                            }
                        },
                        traseras: (callback) => {
                            if (storageTraseras.getItem('estado') == 0.5) {
                                console.log('cortinas - bajando traseras desde la mitad')
                                arduino.rearDownON()
                                setTimeout(() => {
                                    arduino.rearDownOFF()
                                    storageTraseras.setItem('estado', 0)
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageTraseras.getItem('estado') == 1) {
                                console.log('cortinas - bajando traseras')
                                arduino.rearDownON()
                                setTimeout(() => {
                                    arduino.rearDownOFF()
                                    storageTraseras.setItem('estado', 0)
                                    callback(null, true)
                                }, config.tiempo_cortinas)
                            } else if (storageTraseras.getItem('estado') == 0) {
                                callback(null, true)
                            }
                        },
                        gradas: (callback) => {
                            if (storageGradas.getItem('estado') == 0.5) {
                                console.log('cortinas - bajando gradas desde la mitad')
                                arduino.stepsDownON()
                                setTimeout(() => {
                                    arduino.stepsDownOFF()
                                    storageGradas.setItem('estado', 0)
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageGradas.getItem('estado') == 1) {
                                console.log('cortinas - bajando gradas')
                                arduino.stepsDownON()
                                setTimeout(() => {
                                    arduino.stepsDownOFF()
                                    storageGradas.setItem('estado', 0)
                                    callback(null, true)
                                }, config.tiempo_cortinas)
                            } else if (storageGradas.getItem('estado') == 0) {
                                callback(null, true)
                            }
                        }
                    }, (err, result) => {
                        if (err) reject()
                        if (result.frontales && result.laterales && result.traseras && result.gradas) resolve()
                        else reject()
                    })
                    break

                case 'mitad':

                    async({
                        frontales: (callback) => {
                            if (storageFrontales.getItem('estado') == 1) {
                                console.log('cortinas - bajando frontales hacia la mitad')
                                arduino.frontDownON()
                                setTimeout(() => {
                                    arduino.frontDownOFF()
                                    storageFrontales.setItem('estado', 0.5) // 0.5 = mitad                
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageFrontales.getItem('estado') == 0) {
                                console.log('cortinas - subiendo frontales hacia la mitad')
                                arduino.frontUpON()
                                setTimeout(() => {
                                    arduino.frontUpOFF()
                                    storageFrontales.setItem('estado', 0.5) // 0.5 = mitad                
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageFrontales.getItem('estado') == 0.5) {
                                callback(null, true)
                            }
                        },
                        laterales: (callback) => {
                            if (storageLaterales.getItem('estado') == 1) {
                                console.log('cortinas - bajando laterales hacia la mitad')
                                arduino.sideDownON()
                                setTimeout(() => {
                                    arduino.sideDownOFF()
                                    storageLaterales.setItem('estado', 0.5) // 0.5 = mitad                
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageLaterales.getItem('estado') == 0) {
                                console.log('cortinas - subiendo laterales hacia la mitad')
                                arduino.sideUpON()
                                setTimeout(() => {
                                    arduino.sideUpOFF()
                                    storageLaterales.setItem('estado', 0.5) // 0.5 = mitad                
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageLaterales.getItem('estado') == 0.5) {
                                callback(null, true)
                            }
                        },
                        traseras: (callback) => {
                            if (storageTraseras.getItem('estado') == 1) {
                                console.log('cortinas - bajando traseras hacia la mitad')
                                arduino.rearDownON()
                                setTimeout(() => {
                                    arduino.rearDownOFF()
                                    storageTraseras.setItem('estado', 0.5) // 0.5 = mitad                
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageTraseras.getItem('estado') == 0) {
                                console.log('cortinas - subiendo traseras hacia la mitad')
                                arduino.rearUpON()
                                setTimeout(() => {
                                    arduino.rearUpOFF()
                                    storageTraseras.setItem('estado', 0.5) // 0.5 = mitad                
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageTraseras.getItem('estado') == 0.5) {
                                callback(null, true)
                            }
                        },
                        gradas: (callback) => {
                            if (storageGradas.getItem('estado') == 1) {
                                console.log('cortinas - bajando gradas hacia la mitad')
                                arduino.stepsDownON()
                                setTimeout(() => {
                                    arduino.stepsDownOFF()
                                    storageGradas.setItem('estado', 0.5) // 0.5 = mitad                
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageGradas.getItem('estado') == 0) {
                                console.log('cortinas - subiendo gradas hacia la mitad')
                                arduino.stepsUpON()
                                setTimeout(() => {
                                    arduino.stepsUpOFF()
                                    storageGradas.setItem('estado', 0.5) // 0.5 = mitad                
                                    callback(null, true)
                                }, config.tiempoMitad_cortinas)
                            } else if (storageGradas.getItem('estado') == 0.5) {
                                callback(null, true)
                            }
                        }
                    }, (err, result) => {
                        if (err) reject()
                        if (result.frontales && result.laterales && result.traseras && result.gradas) resolve()
                        else reject()
                    })
                    break

                default:
                    reject({
                        status: 400,
                        message: 'accion no disponible'
                    })
                    break

            }
        } catch (e) {
            reject({
                status: 500,
                message: `error al procesas cortinas: todas - ${e}`
            })
        }
    })
}

module.exports = cortina