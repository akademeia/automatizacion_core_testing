'use strict'

const arduino = require('../')
const config = require('../../config')
const LocalStorage = require('node-localstorage').LocalStorage
let localStorage = new LocalStorage(`${config.proyect_location}/storage/storage_cortinas_traseras`)

localStorage.setItem('estado', config.default_estado_cortinas) // estado inicial

function cortina(accion) {

    return new Promise((resolve, reject) => {

        try {

            switch (accion) {

                case 'subir':
                    if (localStorage.getItem('estado') == 0.5) {
                        console.log('cortinas - subiendo traseras desde mitad')
                        arduino.rearUpON()
                        setTimeout(() => {
                            arduino.rearUpOFF()
                            localStorage.setItem('estado', 1) // 1 = arriba                     
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        console.log('cortinas - subiendo traseras')
                        arduino.rearUpON()
                        setTimeout(() => {
                            arduino.rearUpOFF()
                            localStorage.setItem('estado', 1) // 1 = arriba
                            resolve()
                        }, config.tiempo_cortinas)
                    } else if (localStorage.getItem('estado') == 1) {
                        resolve()
                    }
                    break

                case 'bajar':
                    if (localStorage.getItem('estado') == 0.5) {
                        console.log('cortinas - bajando traseras desde la mitad')
                        arduino.rearDownON()
                        setTimeout(() => {
                            arduino.rearDownOFF()
                            localStorage.setItem('estado', 0) // 0 = abajo                            
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 1) {
                        console.log('cortinas - bajando traseras')
                        arduino.rearDownON()
                        setTimeout(() => {
                            arduino.rearDownOFF()
                            localStorage.setItem('estado', 0) // 0 = abajo
                            resolve()
                        }, config.tiempo_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        resolve()
                    }
                    break

                case 'mitad':
                    if (localStorage.getItem('estado') == 1) {
                        console.log('cortinas - bajando traseras hacia la mitad')
                        arduino.rearDownON()
                        setTimeout(() => {
                            arduino.rearDownOFF()
                            localStorage.setItem('estado', 0.5) // 0.5 = mitad                
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        console.log('cortinas - subiendo traseras hacia la mitad')
                        arduino.rearUpON()
                        setTimeout(() => {
                            arduino.rearUpOFF()
                            localStorage.setItem('estado', 0.5) // 0.5 = mitad                
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0.5) {
                        resolve()
                    }
                    break

                default:
                    reject({
                        status: 400,
                        message: 'accion no disponible'
                    })
                    break

            }
        } catch (e) {
            reject({
                status: 500,
                message: 'error al procesas cortinas: traseras'
            })
        }
    })
}

module.exports = cortina