'use strict'

const arduino = require('../')
const config = require('../../config')
const LocalStorage = require('node-localstorage').LocalStorage
let localStorage = new LocalStorage(`${config.proyect_location}/storage/storage_cortinas_frontales`)

localStorage.setItem('estado', config.default_estado_cortinas) // estado inicial

function cortina(accion) {
    return new Promise((resolve, reject) => {

        try {

            switch (accion) {

                case 'subir':
                    if (localStorage.getItem('estado') == 0.5) {
                        console.log('cortinas - subiendo frontales desde mitad')
                        arduino.frontUpON()
                        setTimeout(() => {
                            arduino.frontUpOFF()
                            localStorage.setItem('estado', 1) // 1 = arriba                     
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        console.log('cortinas - subiendo frontales')
                        arduino.frontUpON()
                        setTimeout(() => {
                            arduino.frontUpOFF()
                            localStorage.setItem('estado', 1) // 1 = arriba
                            resolve()
                        }, config.tiempo_cortinas)
                    } else if (localStorage.getItem('estado') == 1) {
                        resolve()
                    }
                    break

                case 'bajar':
                    if (localStorage.getItem('estado') == 0.5) {
                        console.log('cortinas - bajando frontales desde la mitad')
                        arduino.frontDownON()
                        setTimeout(() => {
                            arduino.frontDownOFF()
                            localStorage.setItem('estado', 0) // 0 = abajo                            
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 1) {
                        console.log('cortinas - bajando frontales')
                        arduino.frontDownON()
                        setTimeout(() => {
                            arduino.frontDownOFF()
                            localStorage.setItem('estado', 0) // 0 = abajo
                            resolve()
                        }, config.tiempo_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        resolve()
                    }
                    break

                case 'mitad':
                    if (localStorage.getItem('estado') == 1) {
                        console.log('cortinas - bajando frontales hacia la mitad')
                        arduino.frontDownON()
                        setTimeout(() => {
                            arduino.frontDownOFF()
                            localStorage.setItem('estado', 0.5) // 0.5 = mitad                
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0) {
                        console.log('cortinas - subiendo frontales hacia la mitad')
                        arduino.frontUpON()
                        setTimeout(() => {
                            arduino.frontUpOFF()
                            localStorage.setItem('estado', 0.5) // 0.5 = mitad                
                            resolve()
                        }, config.tiempoMitad_cortinas)
                    } else if (localStorage.getItem('estado') == 0.5) {
                        resolve()
                    }
                    break

                default:
                    reject({
                        status: 400,
                        message: 'accion no disponible'
                    })
                    break

            }
        } catch (e) {
            reject({
                status: 500,
                message: 'error al procesas cortinas: frontales'
            })
        }
    })
}

module.exports = cortina