'use strict'

const express = require('express')
const irCtrl = require('../controllers/ir')
const aireCtrl = require('../controllers/aire')
const rs232Ctrl = require('../controllers/rs232')
const volumenCtrl = require('../controllers/volumen')
const config = require('../config')
const api = express.Router();

api.get('/test', (req, res) => {
    res.status(200).send({ message: 'testing the core' })
});

api.post('/ir', irCtrl)
api.post('/rs232/:code', rs232Ctrl.sendCommand)
api.post('/aire/:cantidad', aireCtrl);
api.post('/volumen', volumenCtrl);

if(config.arduino == 1){
    const cortinaCtrl = require('../controllers/cortina')
    api.post('/cortinas/:idCortina/:accion', cortinaCtrl)
}

module.exports = api